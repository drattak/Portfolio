<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <title>Vincent EMILE</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link rel="stylesheet" href="web/css/style.css">

    <!--<script src="components/jquery.js"></script>-->
    <script src="https://code.jquery.com/jquery-2.2.0.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://code.angularjs.org/1.5.0/angular.js"></script>
    <script src="https://code.angularjs.org/1.5.0/angular-route.js"></script>
    <script type="text/javascript" src="script/particles.min.js"></script>

    <script src="script/app.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
</head>
<body ng-app="app">
<div id="particles-js"></div>

<nav class="navbar-computer">
    <ul>
        <li class="li-nav" id="li-home"><a href="#/">
                <button class="btn-nav" id="homebtn">Accueil</button>
            </a></li>
        <li class="li-nav" id="tech"><a href="#/technos">
                <button class="btn-nav">Technologies</button>
            </a></li>
        <li class="li-nav" id="work"><a href="#/exp">
                <button class="btn-nav">Expériences Professionnelles</button>
            </a></li>
        <li class="li-nav" id="projects"><a href="#/realisations">
                <button class="btn-nav">Réalisations</button>
            </a></li>
        <li class="li-nav" id="contact"><a href="#/contact">
                <button class="btn-nav">Contact</button>
            </a></li>
        <i id="responsiveTrigger" class="fa fa-bars" aria-hidden="true"></i>
    </ul>
</nav>
<nav class="navbar-responsive">
    <ul>
        <li class="li-nav" id="li-home"><a href="#/">
                <button class="btn-nav" id="homebtn">Accueil</button>
            </a></li>
        <li class="li-nav" id="tech"><a href="#/technos">
                <button class="btn-nav">Technologies</button>
            </a></li>
        <li class="li-nav" id="work"><a href="#/exp">
                <button class="btn-nav">Expériences Professionnelles</button>
            </a></li>
        <li class="li-nav" id="projects"><a href="#/realisations">
                <button class="btn-nav">Réalisations</button>
            </a></li>
        <li class="li-nav" id="contact"><a href="#/contact">
                <button class="btn-nav">Contact</button>
            </a></li>
    </ul>
</nav>

<div id="greyed">

</div>
<div class="alert alert-success" id="mail-success">
    <strong>Félicitations!</strong> Le mail a correctement été envoyé.
</div>
<div class="alert alert-danger" id="mail-failure">
    <strong>Attention!</strong> Veuillez remplir tous les champs.
</div>
<div ng-view></div>
</body>
<script src="script/script.js"></script>

</html>

<?php
if (isset($_GET['mail']) && $_GET['mail'] === 'success') {
    echo "<script type='text/javascript'>$('#mail-success').fadeIn();
setTimeout(function(){
$('#mail-success').fadeOut()}
,3000);</script>";
}
?>