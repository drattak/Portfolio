<?php
/**
 * Created by PhpStorm.
 * User: U468006
 * Date: 30/03/2016
 * Time: 11:38
 */
if (isset($_GET['type'])) {
    if ($_GET['type'] !== '') {
        $file_url = '../web/download/cv.'.$_GET['type'];
        if ($_GET['type'] === 'pdf') {
            header('Content-Type: application/pdf');
        } else {
            header('Content-Type: application/octet-stream');
        }
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"Vincent EMILE " . basename($file_url) . "\"");
        readfile($file_url);
    }
}


