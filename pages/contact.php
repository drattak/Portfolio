<h1 class="main-title">Comment me contacter ?</h1>

<ul class="social main">
    <li><a href="http://lnkdin.me/vemile" target="_blank"><img src="web/img/linkedin.png"  class="logo" alt=""></a></li>
    <li><a href="https://plus.google.com/+VincentEmile" target="_blank"><img src="web/img/gplus.png" alt="" class="logo"></a></li>

    <!--<li data-toggle="modal" data-target="#coordonnees"><img src="web/img/phone.png" alt="" class="logo">Afficher les coordonnées</li>-->
    <form id="form-mail" action="srv/mail.php" method="post">
        <label class="contact-form" for="name">Nom</label>
        <input type="text" id="name" placeholder="DUPONT Jean-Philippe" class="form-control" name="name">
        <label class="contact-form" for="mail">Mail</label>
        <input type="email" id="mail" placeholder="dupont@jp.com" class="form-control" name="mail">
        <label class="contact-form" for="message">Message</label>
        <textarea name="message" id="message" cols="30" rows="10" class="form-control"></textarea>
        <button type="submit" id="send" class="btn btn-success">Envoyer</button>
    </form>




    <a href="mailto:contact@vincentemile.fr" id="send-mail-button"><button class="btn btn-primary">Envoyer un mail</button></a>
</ul>
<?php
if(isset($_GET['invalid']) && $_GET['invalid']=='true'){
    echo "<script>alert('Le formulaire est invalide')</script>";
}
