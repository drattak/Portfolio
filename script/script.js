/*global
 $, alert, console, particlesJS
 */
"use strict";
function init_navbar() {
    $('.btn-nav').click(function (event) {
        $('.btn-nav').each(function () {
            if (this === event.target) {
                $(this).css('color', 'grey');
            }
            else {
                $(this).css('color', 'darkgrey');
            }
        });
    });
}
init_navbar();
/*function triggerResponsive(){
 $('.li-language').css('width','100%');
 $('.li-nav').css('display','block');
 $('.navbar').css('height','auto');
 $('.navbar').find('ul').css('height','auto');
 $('.li-nav').css('font-size','14px');

 }

 function unTriggerResponsive(){

 $('.li-language').removeAttr('style');
 $('.li-nav').css('display','inline');
 $('.navbar').css('height','50px');
 $('.navbar').find('ul').css('height','50px');
 }
 */
$(window).resize(function () {
    if ($(window).width() <= 1000) {
        $('.navbar-computer').find('i').show();
        $('.navbar-computer .li-nav').hide();
    }
    else {
        $('.navbar-computer').find('i').hide();
        $('.navbar-responsive').hide();
        $('#greyed').hide();
        $('.navbar-computer .li-nav').show();
    }

});
/*
 $(window).load(function(){
 if(window.innerWidth <= 700) {}
 });*/

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('.navbar').find('i').show();
    $('.li-nav').hide();
}



$(function () {
    $('#send').click(function () {
        $.ajax({
            url: '../srv/mail.php',
            type: 'post',
            data: {mail: $('#mail').val(), name: $('#name').val(), content: $('#message').val()},
            success: function () {
                $('#mail-success').fadeIn();
            }
        });
    });
    /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
    particlesJS.load('particles-js', 'assets/particles.json', function () {
        console.log('callback - particles.js config loaded');
    });

    $('#greyed').click(function () {
        $('.navbar-responsive').hide();
        $('#greyed').fadeOut(200);
    });
});

$(window).load(function () {
    if ($(window).width() <= 1000) {
        $('.navbar-computer').find('i').show();
        $('.navbar-computer .li-nav').hide();
    }
});

$('.navbar-computer').find('i').click(function () {
    $('.navbar-responsive').show();
    $('#greyed').fadeIn(200);
});

function age() {
    var birthdate = new Date();
    birthdate.setTime(841305934664);
    var bd = birthdate.getTime();
    var res = new Date().getTime() - bd;
    console.log(parseInt(res / 1000 / 60 / 60 / 24 / 365, 10));
    $(".age")[0].innerHTML = parseInt(res / 1000 / 60 / 60 / 24 / 365, 10) + " ans";
}