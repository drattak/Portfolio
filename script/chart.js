/**
 * Created by drattak on 31/01/16.
 */
/*global
$,
 */
$(function () {
    "use strict";
    $('#container').highcharts({

        chart: {
            polar: true,
            type: 'line',
            backgroundColor: 'rgba(255, 255, 255, 0.1)',
            renderTo: 'container'
        },

        title: {
            text: '',
            x: -80
        },

        pane: {
            size: '80%'
        },

        xAxis: {
            categories: ['HTML','CSS', 'Javascript',  'Java', 'SQL', 'Git','PHP', 'Programmation Android'
               ],
            labels: {style: {color: '#111', fontWeight: 'bold',fontSize: '20px'}}

        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y}%</b><br/>'
        },

        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 70,
            layout: 'vertical'
        },

        series: [{
            showInLegend: false,
            data: [90, 90, 80,  80, 90, 80, 80,75],
            pointPlacement: 'on'
        }]

    });
});