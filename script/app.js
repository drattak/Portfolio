/**
 * Created by drattak on 31/01/16.
 */
var app = angular.module('app', ['ngRoute']);

app.config(function($routeProvider) {
    $routeProvider

        .when('/', {
            templateUrl : 'pages/home.html'
        })

        .when('/technos', {
            templateUrl : 'pages/technos.html',
            controller  : 'techController'
        })

        .when('/contact', {
            templateUrl : 'pages/contact.php',
            controller  : 'contactController'
        })

        .when('/realisations', {
            templateUrl : 'pages/realisations.html',
            controller  : 'projetsController'
        })
        .when('/exp', {
            templateUrl : 'pages/exp.html',
            controller  : 'expController'
        })
});

app.controller('expController',['$scope',function($scope){
    $scope.title = "Expériences Professionnelles";
}]);

app.controller('techController',['$scope', function ($scope) {
    $scope.title = "Technologies Utilisées";
}]);